<?php
namespace controller\interfaces;

interface AmountInterface {
    public function getAmount();
    public function getTax($amount);
    public function getVat($tax_amount);

}











