<?php
namespace Controller;
include("interfaces/AmountInterface.php");
//require("../Model/DatabaseConnection.php");
//use app\interfaces\AmountInterface;
use controller\interfaces\AmountInterface;
use app\Model;

class AmountController  implements AmountInterface {

   public $model;
    public function __construct($model)
    {

        $this->model = $model;
    }

    public function index() {
        include '../../View/getAmount.php';
    }
    public function getAmount()
    {
       $amount = $_POST['amount'];
       $data = $this->getTax($amount);
       include '../../View/amount/amount.php';

    }
    public function getTax($amount)
    {
        $tax = $amount*(10/100);
        $vat = $this->getVat($tax);
        $abc_amount = $amount-($tax+$vat);
        return ['tax'=>$tax,'vat'=>$vat,'abc_amount'=>$abc_amount];
    }
    public function getVat($tax_amount)
    {
        $vat = $tax_amount*(12/100);
        return $vat;
    }


}