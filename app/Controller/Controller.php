<?php
namespace App;
//include_once("../Model/Model.php");

class Controller {
    /**
     * The home page controller
     */
    private $model;

    function __construct($model)
    {
        $this->model = $model;
    }

    public function sayWelcome()
    {
        return $this->model->welcomeMessage();
    }


}

