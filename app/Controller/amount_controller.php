<?php

/**
 * Created by PhpStorm.
 * User: chandan
 * Date: 4/3/19
 * Time: 12:14 PM
 */
class AmountController
{
    private $modelObj;
    private $connection;
    public $limit = 4;

    function __construct( $model )
    {
        $this->modelObj = $model;
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "phpmvc";
        // Create connection
        $this->connection = new mysqli($servername, $username, $password, $dbname);

    }
    public function index() {

        include 'View/getAmount.php';
    }
    public function getAmount()
    {
        $amount = $_POST['amount'];
        $data = $this->getTax($amount);
        $this->insertData($data,$amount);
        $getTransaction = $this->getTransaction();
        //print_r($getTransaction);die;
        include 'View/amount/amount.php';

    }
    public function getTax($amount)
    {
        $tax = $amount*(10/100);
        $vat = $this->getVat($tax);
        $abc_amount = $amount-($tax+$vat);
        return ['tax'=>$tax,'vat'=>$vat,'abc_amount'=>$abc_amount];
    }
    public function getVat($tax_amount)
    {
        $vat = $tax_amount*(12/100);
        return $vat;
    }

    public function insertData($data,$amount)
    {
        $tax=$data['tax'];
        $vat=$data['vat'];

        if ($this->connection->connect_error) {
            return 'error';
        }

        $sql = "INSERT INTO transaction (amount, tax, vat) VALUES ($amount,$tax,$vat)";
        if ($this->connection->query($sql) == true) {
            return "Data Inserted";
        } else {
            return "Failed";
        }
    }

    public function getTransaction() {
        if ($this->connection->connect_error) {
            return 'error';
        }
        $this->limit = 4;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $this->limit;
        $sql = "SELECT * FROM transaction ORDER BY ID DESC LIMIT $start_from, $this->limit";
        if ($this->connection->query($sql) == true) {
            return $this->connection->query($sql);
        } else {
            return "Failed";
        }
    }

}