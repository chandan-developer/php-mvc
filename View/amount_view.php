<?php
/**
 * The about page view
 */
class AmountView
{

    private $modelObj;

    private $controller;


    function __construct($controller, $model)
    {
        $this->controller = $controller;

        $this->modelObj = $model;

    }

    public function enterAmount()
    {

        return $this->controller->index();
    }

    public function getAmount()
    {

        return $this->controller->getAmount();
    }

    

}