<!DOCTYPE html>
<html lang="en">
<head>
    <title>Tax Calculation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>TAX CALCULATION</h2>
    <table class="table">
        <thead>
        <tr>
            <th>Amount Paid</th>
            <th>TAX</th>
            <th>VAT</th>
            <th>ABC AMOUNT</th>
        </tr>
        </thead>
        <tbody>
        <?php


        $total_amount = 0;
        $total_tax = 0;
        $total_vat = 0;
        $total_abc_amount = 0;
        while($data1 = $getTransaction->fetch_assoc()){
           $class_array = ['success','warning','info','danger'];
           $class = array_rand($class_array,4);

            ?>

            <tr class=<?php echo $class_array[$class[rand(0,4)]] ?>>
            <td><?php echo $data1['amount'] ?></td>
        <td><?php echo $data1['tax'] ?></td>
        <td><?php echo $data1['vat'] ?></td>
        <td><?php echo $data1['amount']-($data1['vat']+$data1['tax']) ?></td>
        </tr>
        <?php
        $total_amount= $total_amount + $data1['amount'];
            $total_tax= $total_tax + $data1['tax'];
            $total_vat= $total_vat + $data1['vat'];
        $total_abc_amount= $total_abc_amount +$data1['amount']-($data1['vat']+$data1['tax']);
        } ?>
        </tbody>

    </table>

         <p> Total Paid Amount: <?php echo $total_amount ?></p>
         <p> Total Tax Amount: <?php echo $total_tax ?></p>
         <p> Total Vat Amount: <?php echo $total_vat ?></p>
         <p> Total ABC Amount: <?php echo $total_abc_amount ?></p>

</div>

</body>
</html>

