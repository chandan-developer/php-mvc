<?php
namespace App;
//include "../app/Model/Model.php";
//use app\Model\Model;
/**
* The home page view
*/
    class View
    {

        private $model;

        private $controller;


        function __construct($controller, $model)
        {
            $this->controller = $controller;

            $this->model = $model;

            print "Home - ";
        }

        public function index()
        {

            return $this->controller->sayWelcome();
        }

        public function action()
        {
            return $this->controller->takeAction();
        }

    }
